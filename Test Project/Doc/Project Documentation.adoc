= CML DQMH.lvproj
LabVIEW Doc Generator Test
:toc: 
:imagesdir: Images

== CML UI.lvlib

Responsability: To be retrieve from lvlib description

Type: Singleton

== Acquisition.lvlib

Responsability: To be retrieve from lvlib description

Type: Singleton

=== Stop Acquiring

image::Stop Acquiring.png[Stop Acquiring]

Type: Request

Requests the Acquisition Module to stop acquiring


=== Calibrate DAQ

image::Calibrate DAQ.png[Calibrate DAQ]

Type: Request

Add calibration routine, and linear slope and offset to the acquisition helper loop.


=== Start Acquiring

image::Start Acquiring.png[Start Acquiring]

Type: Request

<b>Note</b>: This VI was renamed by the DQMH Rename Event utility. Make sure the VI Description is updated to reflect the new event name, then delete this comment.

Requests Acquisition Module to start acquiring.


=== Acquisition Started

image::Acquisition Started.png[Acquisition Started]

Type: Broadcast

Broadcasts that the Acquisition Module started acquiring


=== Acquisition Stopped

image::Acquisition Stopped.png[Acquisition Stopped]

Type: Broadcast

Broadcasts that the Acquisition Module has stopped acquiring data.


=== Data Updated

image::Data Updated.png[Data Updated]

Type: Broadcast

Broadcasts the latest data acquired


=== Device Calibrated

image::Device Calibrated.png[Device Calibrated]

Type: Broadcast

Broadcasts that the Acquisition module calibrated the device.


== Logger.lvlib

Responsability: To be retrieve from lvlib description

Type: Singleton

=== Initialize File

image::Initialize File.png[Initialize File]

Type: Request

Request the Logger to initialize the file and report when the Logger has initialized the file.


=== Log Data

image::Log Data.png[Log Data]

Type: Request

<b>Note</b>: This VI was renamed by the DQMH Rename Event utility. Make sure the VI Description is updated to reflect the new event name, then delete this comment.

Requests Logger module to log data to file


=== Stop Logging

image::Stop Logging.png[Stop Logging]

Type: Request

Requests Logger module to stop logging and the module broadcasts when the logging has stopped.


=== Logging Stopped

image::Logging Stopped.png[Logging Stopped]

Type: Broadcast

Requests Logger module to stop logging and the module broadcasts when the logging has stopped.


== Settings Editor.lvlib

Responsability: To be retrieve from lvlib description

Type: Singleton

=== Update Application Settings

image::Update Application Settings.png[Update Application Settings]

Type: Request

Request Settings Editor to return the latest settings. The Settings Editor can send the applications settings as a reply or via broadcast.

